<?php
namespace app\controllers;

use app\models\Category;
use app\models\Product;
use yii\data\Pagination;
use yii\web\HttpException;

class CategoryController extends AppController
{
    public $title = 'Мой могазин';

    public function actionIndex()
    {
        $hits = Product::find()->where(['hit' => '1'])->limit(6)->all();
        $this->setMeta($this->title);

        return $this->render('index', compact('hits'));
    }

    public function actionView($id)
    {
        $category = Category::findOne($id);

        if(empty($category))
        {
            throw new HttpException(404, 'Страница категории не най дена!');
        }

        $this->setMeta($this->title . '|' . $category->name, $category->keywords, $category->description);

        $query = Product::find()->where(['category_id' => $id]);
        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 3,
            'forcePageParam' => false,
            'pageSizeParam'=> false,
        ]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('view', [
            'category' => $category,
            'products' => $products,
            'pages' => $pages,
        ]);
    }
    
    public function actionSearch($q)
    {
        $q = trim($q);
        $this->setMeta($this->title . ' | Поиск ' . $q);

        if(!$q)
        {
            return $this->render('search');
        }

        $query = Product::find()->where(['like', 'name', $q]);
        $pages = new Pagination([
            'totalCount' => $query->count(),
            'pageSize' => 3,
            'forcePageParam' => false,
            'pageSizeParam'=> false,
        ]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('search', compact('products', 'pages', 'q'));
    }
    
}
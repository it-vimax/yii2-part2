<?php
/**
 * Created by PhpStorm.
 * User: Наталия
 * Date: 24.11.2017
 * Time: 10:08
 */

namespace app\controllers;

use app\models\Product;
use yii\web\HttpException;

class ProductController extends AppController
{
    public $title = "Карточка продукта";

    public function actionView($id)
    {
        $product = Product::findOne($id);

        if(empty($product))
        {
            throw new HttpException(404, 'Страница продукта не найдена!');
        }

        $hits = Product::find()->where(['hit' => '1'])->limit(6)->all();

        $this->setMeta($this->title . '|' . $product->name, $product->keywords, $product->description);

        return $this->render('view', compact('product', 'hits'));
    }
    
}
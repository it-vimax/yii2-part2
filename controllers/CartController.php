<?php
namespace app\controllers;

use app\models\Cart;
use app\models\Order;
use app\models\OrderItems;
use app\models\Product;
use Yii;
use yii\web\Response;

class CartController extends AppController
{

    public function actionAdd()
    {
        $id = Yii::$app->request->get('id');
        $qty = (integer) Yii::$app->request->get('qty');
        $qty = !$qty ? 1 : $qty;
        $product = Product::findOne($id);
        if(empty($product))
        {
            return false;
        }
        session_start();
        $cart = new Cart();
        $cart->addToCart($product, $qty);
        return $this->renderAjax('cart-modal');
    }

    public function actionClear()
    {
        session_start();
        unset($_SESSION['cart']);
        unset($_SESSION['cart.qty']);
        unset($_SESSION['cart.sum']);
        return $this->renderAjax('cart-modal');
    }

    public function actionDelItem()
    {
        $id = Yii::$app->request->get('id');
        session_start();
        $cart = new Cart();
        $cart->recalc($id);
        return $this->renderAjax('cart-modal');
    }
    
    public function actionShow()
    {
        return $this->renderAjax('cart-modal');
    }
    
    public function actionView()
    {
        $this->setMeta('Корзина');
        $order = new Order();
        if($order->load(Yii::$app->request->post()))
        {
            $order->qty = $_SESSION['cart.qty'];
            $order->sum = $_SESSION['cart.sum'];
            if($order->save())
            {
                $this->saveOrderItems($_SESSION['cart'], $order->id);
                Yii::$app->session->setFlash('success', 'Ваш заказ прийнят. Менеджер вскоре свяжится с вами.');
                unset($_SESSION['cart']);
                unset($_SESSION['cart.qty']);
                unset($_SESSION['cart.sum']);
                return $this->refresh();
            }
            else
            {
                Yii::$app->session->setFlash('error', 'Возникла ошибка при формировании заказа!');
            }
        }
        return $this->render('view', compact('order'));
    }

    public function saveOrderItems($items, $orderId)
    {
        $insert = [];
        foreach($items as $id => $item)
        {
            $insert[] = [$orderId, $id, $item['name'], $item['price'], $item['qty'], $item['qty'] * $item['price']];
        }
        Yii::$app->db->createCommand()->batchInsert('order_items', ['order_id', 'product_id', 'name', 'price', 'qty_item', 'sum_item'], $insert)->execute();
    }
}
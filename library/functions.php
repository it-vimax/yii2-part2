<?php

function dump($arr)
{
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
}
function dd($arr)
{
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
    die;
}
function vd($arr)
{
    echo '<pre>';
    var_dump($arr);
    echo '</pre>';
    die;
}
function vdump($arr)
{
    echo '<pre>';
    var_dump($arr);
    echo '</pre>';
}
<?php
/**
 * Created by PhpStorm.
 * User: Наталия
 * Date: 22.11.2017
 * Time: 18:32
 */

namespace app\models;

use yii\db\ActiveRecord;

class Product extends ActiveRecord
{
    public static function tableName()
    {
        return 'product';
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
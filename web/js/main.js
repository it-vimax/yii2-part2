/*price range*/

 $('#sl2').slider();

	var RGBChange = function() {
	  $('#RGB').css('background', 'rgb('+r.getValue()+','+g.getValue()+','+b.getValue()+')')
	};	
		
/*scroll to top*/

$(document).ready(function()
{
    $('.catalog').dcAccordion({
		speed: 300
	});

    $('.add-to-cart').on("click", function(event)
    {
        event.preventDefault();
        var id = $(this).data('id');
        var qty = $('#qty').val();
        console.log('id = ' + id);
        console.log('qty = ' + qty);
        $.ajax({
            url: '/cart/add',                 // указываем URL
        	//headers: {"Authorization": "Basic " + USERNAME + ":" + PASSWORD},	//	отправка заголовка запроса
            // dataType : "json",	        // тип данных возвращаемых в callback функцию (xml, html, script, json, text, _default)
        	data : {id : id, qty: qty},	            //	даные, которые передаем
        	async : true,	            //	асинхронность запроса, по умолчанию true
        	cache : true,	            //	вкл/выкл кэширование данных браузером, по умолчанию true
        	contentType : "application/x-www-form-urlencoded",
        	type : "get",              // GET либо POST

            success: function (data)
        	{	// вешаем свой обработчик на функцию success
				if(!data)
				{
					console.log('не найден товар!');

					return;
				}
				showCart(data);
            },

        	error: function (error)
        	{	// вешаем свой обработчик на функцию error
        		alert('Error!');
        	},

        	beforeSend: function(){},	//	срабатывает перед отправкой запроса
        	complete: function(){}		//	срабатывает по окончанию запроса
        });

		$("#cart .modal-body").on("click", ".del-item", function(event)
		{
		    event.preventDefault();
		    var id = $(this).data('id');
		    $.ajax({
		        url: '/cart/del-item',                 // указываем URL
		    	//headers: {"Authorization": "Basic " + USERNAME + ":" + PASSWORD},	//	отправка заголовка запроса
		        //dataType : "json",	        // тип данных возвращаемых в callback функцию (xml, html, script, json, text, _default)
		    	data : {id: id},	            //	даные, которые передаем
		    	async : true,	            //	асинхронность запроса, по умолчанию true
		    	cache : true,	            //	вкл/выкл кэширование данных браузером, по умолчанию true
		    	contentType : "application/x-www-form-urlencoded",
		    	type : "get",            // GET либо POST
		    	
		        success: function (data)
		    	{	// вешаем свой обработчик на функцию success
		    	    if(!data)
		    	    {
		    	        console.warn('false');
		    	    }
                    showCart(data);
		        },
		    	
		    	error: function (error)
		    	{	// вешаем свой обработчик на функцию error
		    		console.log(error);
		    	},
		    	
		    	beforeSend: function(){},	//	срабатывает перед отправкой запроса
		    	complete: function(){}		//	срабатывает по окончанию запроса
		    });
		    
		});
		console.log('click del-item');
    });

	$(function () {
		$.scrollUp({
	        scrollName: 'scrollUp', // Element ID
	        scrollDistance: 300, // Distance from top/bottom before showing element (px)
	        scrollFrom: 'top', // 'top' or 'bottom'
	        scrollSpeed: 300, // Speed back to top (ms)
	        easingType: 'linear', // Scroll to top easing (see http://easings.net/)
	        animation: 'fade', // Fade, slide, none
	        animationSpeed: 200, // Animation in speed (ms)
	        scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
					//scrollTarget: false, // Set a custom target element for scrolling to the top
	        scrollText: '<i class="fa fa-angle-up"></i>', // Text for element, can contain HTML
	        scrollTitle: false, // Set a custom <a> title if required.
	        scrollImg: false, // Set true to use image
	        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
	        zIndex: 2147483647 // Z-Index for the overlay
		});
	});
});


function clearCart()
{
	$.ajax({
	    url: '/cart/clear',                  // указываем URL
		//headers: {"Authorization": "Basic " + USERNAME + ":" + PASSWORD},	//	отправка заголовка запроса
        // dataType : "json",	        // тип данных возвращаемых в callback функцию (xml, html, script, json, text, _default)
		data : '',	            //	даные, которые передаем
		async : true,	            //	асинхронность запроса, по умолчанию true
		cache : true,	            //	вкл/выкл кэширование данных браузером, по умолчанию true
		contentType : "application/x-www-form-urlencoded",
		type : "get",            // GET либо POST

	    success: function (data)
		{	// вешаем свой обработчик на функцию success
            showCart(data);
	    },

		error: function (error)
		{	// вешаем свой обработчик на функцию error
			console.log(error);
		},

		beforeSend: function(){},	//	срабатывает перед отправкой запроса
		complete: function(){}		//	срабатывает по окончанию запроса
	});

}

function showCart(cart)
{
    $('#cart .modal-body').html(cart);
    $('#cart').modal();
}

function getCart()
{
    $.ajax({
        url: '/cart/show',                 // указываем URL
    	//headers: {"Authorization": "Basic " + USERNAME + ":" + PASSWORD},	//	отправка заголовка запроса
        //dataType : "json",	        // тип данных возвращаемых в callback функцию (xml, html, script, json, text, _default)
    	data : '',	            //	даные, которые передаем
    	async : true,	            //	асинхронность запроса, по умолчанию true
    	cache : true,	            //	вкл/выкл кэширование данных браузером, по умолчанию true
    	contentType : "application/x-www-form-urlencoded",
    	type : "get",            // GET либо POST
    	
        success: function (data)
    	{	// вешаем свой обработчик на функцию success
    	    if(!data)
    	    {
    	        console.warn('false');
    	    }
            showCart(data);
        },
    	
    	error: function (error)
    	{	// вешаем свой обработчик на функцию error
    		console.log(error);
    	},
    	
    	beforeSend: function(){},	//	срабатывает перед отправкой запроса
    	complete: function(){}		//	срабатывает по окончанию запроса
    });
    return false;
}
